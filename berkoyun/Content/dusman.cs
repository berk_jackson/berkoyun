﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace berkoyun
{
  
    class dusman
    {
        Point position;
        int health;
        int tip;
        int atesZamani;
        public static int kills;
        bool dead;
        int yon;
        int adım = 0;
        
        public dusman(int Type,Point pos,GameTime time)
        {
            dead=false;
            tip=Type;
            if (Type == 0)
            {
                health = 500;
            }
            else if (Type == 1)
            {
                health = 1500;
            }
            else if (Type == 2)
            {
                health = 1000;
            }
            else if (Type == 3)
            {
                health = 1500;
            }
            else if (Type == 4)
            {
                health = 2000;
            }
            else if (Type == 5)
            {
                health = 2500;
            }
            position = pos;
            atesZamani = time.TotalGameTime.Seconds + 1;
            Random rnd = new Random();
           yon= rnd.Next(0, 2);
        }
        public void Update(ref dusman d,GameTime time,ref Oyuncu o)
        {
            if (time.TotalGameTime.Seconds == atesZamani)
            {
                atesEt( ref o);
                atesZamani = time.TotalGameTime.Seconds + 1;
            }
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                
                Rectangle r = new Rectangle(position.X-50, position.Y-70, 42, 150);
              
                if(r.Intersects(new Rectangle(Mouse.GetState().Position.X,Mouse.GetState().Position.Y,1,1)))
                {
                   
                health -= 100;
                }
            }
            if (health == 0)
            {

                dusman.kills++;
              dead=true;

            }
            if(yon==0)
            {
                if (position.X > 0)
                {
                    position.X -= 2;

                }
                else
                    position.X += 2;
                adım++;
            }
            else if(yon==1)
            {
                if (position.X < 950)
                {
                    position.X += 2;

                }
                else
                    position.X -= 2;
                adım++;
            }
            if(adım==20)
            {

                Random rnd = new Random();
                yon = rnd.Next(0, 2);
                adım = 0;
            }

          
        }
        public Point getPosition()
        {
            return position;
        }

        public int getType()
        {
            return tip;
        }
        public void atesEt(ref Oyuncu o)
        {
            o.canAzalt();

        }
        public bool vuruldu()
        {
            return dead;
        }

    }
}
