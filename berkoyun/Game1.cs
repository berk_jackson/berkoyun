﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections;
using System;

namespace berkoyun
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D arkaTex;
        Texture2D hedefText;
        Rectangle arkaRec;
        MouseState mouse;
        Texture2D dusmanText0;
        Texture2D dusmanText1;
        Texture2D dusmanText2;
        Texture2D dusmanText3;
        Texture2D dusmanText4;
        Texture2D dusmanText5;
        Texture2D can1;
        Texture2D can2;
        Texture2D can3;
        Texture2D can4;
        Texture2D can5;
        Texture2D kan;
        Point[] koordinatlar;
        dusman[] dusmanlar;
        double sonraki = 3.00;
        int dusmanIndex = 0;
        Oyuncu oyuncu;
        public static bool gameover = false;
        Texture2D gameovertext;
        SpriteFont font;
        SoundEffect ates;
        bool restartHover = false;
       GameTime oyunZamani;
       SoundEffect ahh;
        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
           
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 989;
            graphics.PreferredBackBufferHeight = 551;
            graphics.ApplyChanges();
            dusmanlar = new dusman[100];
            oyuncu = new Oyuncu();
            koordinatlar = new Point[7];
            koordinatlar[0] = new Point(239, 390);
            koordinatlar[1] = new Point(87, 160);
            koordinatlar[2] = new Point(536,319);
            koordinatlar[3] = new Point(785, 160);     
            koordinatlar[4] = new Point(415, 140);
            koordinatlar[5] = new Point(912, 284);
            koordinatlar[6] = new Point(623, 445);
            dusman.kills = 0;
           
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            arkaTex = Content.Load<Texture2D>("Sprites/countermap");
            hedefText=Content.Load<Texture2D>("Sprites/hedef");
            dusmanText0=Content.Load<Texture2D>("Sprites/0");
            dusmanText1 = Content.Load<Texture2D>("Sprites/1");
            dusmanText2 = Content.Load<Texture2D>("Sprites/2");
            dusmanText3 = Content.Load<Texture2D>("Sprites/3");
            dusmanText4 = Content.Load<Texture2D>("Sprites/4");
            dusmanText5 = Content.Load<Texture2D>("Sprites/5");
            gameovertext = Content.Load<Texture2D>("Sprites/gameover");
            can1 = Content.Load<Texture2D>("Sprites/c1");
            can2 = Content.Load<Texture2D>("Sprites/c2");
            can3 = Content.Load<Texture2D>("Sprites/c3");
            can4 = Content.Load<Texture2D>("Sprites/c4");
            can5 = Content.Load<Texture2D>("Sprites/c5");
            font = Content.Load<SpriteFont>("Fonts/font");
            kan = Content.Load<Texture2D>("Sprites/kan");
            ates = Content.Load<SoundEffect>("Sounds/deagle-2");
            ahh = Content.Load<SoundEffect>("Sounds/ahh");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            oyunZamani = gameTime;
            if (gameover == true)
            {
                Rectangle restart = new Rectangle(350, 250, 120, 50);
                if (restart.Intersects(new Rectangle(Mouse.GetState().Position.X, Mouse.GetState().Position.Y, 1, 1)))
                {
                    restartHover = true;
                }
                else
                {
                    restartHover = false;
                }
              
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    if (restart.Intersects(new Rectangle(Mouse.GetState().Position.X, Mouse.GetState().Position.Y, 1, 1)))
                    {
                        oyunZamani.TotalGameTime = new TimeSpan(0,0,0);
                        sonraki = 3.00;
                        Initialize();
                        gameover = false;
                    }
                }
               
                return;
            }
         
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                float vol = 1f, pitch =-0.001f, pan = 0f;
                ates.Play(vol, pitch, pan);
            }
            
             if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            for (int i = 0; i < dusmanlar.Length; i++)
            {
                
                    if (dusmanlar[i] != null)
                        dusmanlar[i].Update(ref dusmanlar[i],gameTime,ref oyuncu);
                
            }
            // TODO: Add your update logic here
            if (gameTime.TotalGameTime.TotalSeconds >sonraki)
            {
                Random rnd = new Random();
                sonraki = rnd.Next(0, 4) + gameTime.TotalGameTime.TotalSeconds;
                
               rnd = new Random();
               bool found = false;
           int i=rnd.Next(0, 7);
                int type = rnd.Next(0,4);
                for (int j= 0; j < dusmanlar.Length; j++)
                {
                    if (dusmanlar[j] != null)
                    {
                        if (koordinatlar[i] == dusmanlar[j].getPosition())
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (found == false)
                {
                    dusmanlar[dusmanIndex] = new dusman(type, koordinatlar[i], gameTime);
                    dusmanIndex++;
                    if (dusmanIndex == 99)
                    {
                        dusmanIndex = 0;

                    }
                }
               
                base.Update(gameTime);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            // TODO: Add your drawing code here
            spriteBatch.Begin();
            arkaRec = new Rectangle(0, 0, arkaTex.Width, arkaTex.Height);
            if (oyuncu.getVuruldu())
            {
                spriteBatch.Draw(arkaTex, arkaRec, Color.Red);
                oyuncu.setVuruldu(ahh);
            }
            else
            spriteBatch.Draw(arkaTex, arkaRec, Color.White);
            if(oyunZamani.TotalGameTime.TotalSeconds<5)
            spriteBatch.DrawString(font, "Kultur Universitesi Musabakasi basladi :)", new Vector2(280,15), Color.White);

            foreach (dusman d in dusmanlar)
            {
                if (d != null)
                {
                    int tip = d.getType();
                    Texture2D dTipi = null;
                    if (tip == 0)
                        dTipi = dusmanText0;
                    else if (tip == 1)
                        dTipi = dusmanText1;
                    else if (tip == 2)
                        dTipi = dusmanText2;
                    else if (tip == 3)
                        dTipi = dusmanText3;
                    else if (tip == 4)
                        dTipi = dusmanText4;
                    else if (tip == 5)
                        dTipi = dusmanText5;

                    Rectangle rDus = new Rectangle(d.getPosition().X, d.getPosition().Y, dTipi.Width/2, dTipi.Height/2);
                    spriteBatch.Draw(dTipi, rDus, Color.White);
                }
            }
            if (oyuncu.getHealth() >80)
            {
                Rectangle rc = new Rectangle(820, 400, 130, 130);
                spriteBatch.Draw(can1, rc, Color.White);
            }
            else if (oyuncu.getHealth() > 60)
            {
                Rectangle rc = new Rectangle(820, 400, 130, 130);
                spriteBatch.Draw(can2, rc, Color.White);
            }
            else if (oyuncu.getHealth() > 40)
            {
                Rectangle rc = new Rectangle(820, 400, 130, 130);
                spriteBatch.Draw(can3, rc, Color.White);
            }
            else if (oyuncu.getHealth() > 20)
            {
                Rectangle rc = new Rectangle(820, 400, 130, 130);
                spriteBatch.Draw(can4, rc, Color.White);
            }
             else if (oyuncu.getHealth() > 0)
            {
                Rectangle rc = new Rectangle(820, 400, 130, 130);
                spriteBatch.Draw(can5, rc, Color.White);
            }
            spriteBatch.DrawString(font,"Kills: "+ dusman.kills.ToString(), new Vector2(70, 500), Color.White);
            if(!gameover)
            spriteBatch.DrawString(font, "Time:" + ((int)oyunZamani.TotalGameTime.TotalMinutes).ToString() + ":" + oyunZamani.TotalGameTime.Seconds.ToString(), new Vector2(400, 500), Color.White); 
           
                for(int i=0;i<dusmanlar.Length;i++)
                {
                    if (dusmanlar[i] != null)
                    {
                        if (dusmanlar[i].vuruldu())
                        {
                            Rectangle kanr = new Rectangle(Mouse.GetState().Position.X-20, Mouse.GetState().Position.Y, 130, 130);
                            spriteBatch.Draw(kan, kanr, Color.Red);
                            dusmanlar[i] = null;
                        }
                    }
                }


                if (gameover == true)
                {
                   
                    Rectangle rgo = new Rectangle(300,200,gameovertext.Width,gameovertext.Height);
                    spriteBatch.Draw(gameovertext, rgo, Color.White);

                    if (restartHover == true)
                    {
                        spriteBatch.DrawString(font, "Restart", new Vector2(400, 300), Color.Red);
                    }
                    else
                    spriteBatch.DrawString(font, "Restart", new Vector2(400, 300), Color.White);
                 
       
                }
                mouse = Mouse.GetState();

                Rectangle r = new Rectangle(mouse.Position.X, mouse.Position.Y, 100, 100);
                spriteBatch.Draw(hedefText, r, Color.Black);
            spriteBatch.End();
            base.Draw(gameTime);
        }
     
    }
}
